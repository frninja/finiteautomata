﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace AutomataInterface
{
    public class Logger
    {
        private System.Windows.Forms.TextBox _log;

        public Logger(System.Windows.Forms.TextBox log)
        {
            _log = log;
        }

        public void Write(string message)
        {
            _log.AppendText(message);
            _ScrollToEnd();
        }

        public void WriteLine(string message)
        {
            message = new StringBuilder(message).AppendLine().ToString();
            _log.AppendText(message);
            _log.ScrollToCaret();
        }
        
        private void _ScrollToEnd() 
        {
            _log.SelectionStart = _log.Text.Length;
            _log.ScrollToCaret();
        }
    }
}
