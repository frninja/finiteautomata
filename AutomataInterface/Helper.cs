﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

using AutomataLibrary.Automata;
using AutomataLibrary.Parser;
using AutomataLibrary.Parser.AST;
using AutomataLibrary.Builder;

namespace AutomataInterface
{
    public class Helper
    {
        public const string AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT = "Loading failed: {0}",
                            AUTOMATA_LOADING_MESSAGE_FMT = "Loading {0}...",
                            AUTOMATA_LOADING_MESSAGE = "Loading automata...",
                            AUTOMATA_LOAD_OK_MESSAGE = "OK",
                            AUTOMATA_LOADING_COMPLETED_MESSAGE = "Loading completed!",
                            SCAN_HAYSTACK_MESSAGE_FMT = "Scaning \"{0}\"",
                            PATTERN_COUNT_MESSAGE_FMT = "{0} -> {1}",
                            RUN_ON_WORD_MESSAGE_FMT = "Run automata on \"{0}\"...",
                            WORD_ACCEPTED_MESSAGE = "ACCEPTED",
                            WORD_NOT_ACCEPTED_MESSAGE = "NOT ACCEPTED";

        public static IFiniteAutomata LoadAutomata(string path, AutomataBuilder builder)
        {
            using (TextReader textReader = new StreamReader(path))
            {
                AutomataNode automataNode = null;
                if (AutomataParser.Instance().Parse(textReader.ReadToEnd(), out automataNode))
                {
                    if (builder.Build(automataNode))
                    {
                        return builder.GetAutomata();
                    }
                }
            }
            return null;
        }
    }
}
