﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AutomataLibrary.Automata;
using AutomataLibrary.Automata.Decorators;
using AutomataLibrary.Builder;

namespace AutomataInterface.Forms
{
    public partial class Form2 : Form
    {
        private Logger _logger;

        private Dictionary<string, IFiniteAutomataCounterDecorator> _dfas;
        private Dictionary<string, IFiniteAutomataCounterDecorator> _nfas;

        private string[] _patterns = new string[] { "987", "99787", "87799", "977" };

        private bool _automataLoaded = false;

        public Form2()
        {
            InitializeComponent();

            _logger = new Logger(logTextBox);
            _dfas = new Dictionary<string, IFiniteAutomataCounterDecorator>();
            _nfas = new Dictionary<string, IFiniteAutomataCounterDecorator>();

            _LoadAutomata();
        }

        private void _LoadAutomata()
        {
            string dfa_path_fmt = "Automata/part2/dfa_{0}.txt",
                   nfa_path_fmt = "Automata/part2/nfa_{0}.txt";

            foreach (var pattern in _patterns)
            {
                string dfa_name = string.Format(dfa_path_fmt, pattern),
                    nfa_name = string.Format(nfa_path_fmt, pattern);

                _logger.Write(string.Format(Helper.AUTOMATA_LOADING_MESSAGE_FMT, dfa_name));
                try
                {
                    _dfas.Add(pattern, new DFACounterDecorator(Helper.LoadAutomata(dfa_name, new DFABuilder()) as DFA));
                }
                catch (Exception e)
                {
                    _logger.WriteLine(string.Format(Helper.AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT, e.Message));
                    return;
                }
                _logger.WriteLine(Helper.AUTOMATA_LOAD_OK_MESSAGE);

                _logger.Write(string.Format(Helper.AUTOMATA_LOADING_MESSAGE_FMT, nfa_name));
                try
                {
                    _nfas.Add(pattern, new NFACounterDecorator(Helper.LoadAutomata(nfa_name, new NFABuilder()) as NFA));
                }
                catch (Exception e)
                {
                    _logger.WriteLine(string.Format(Helper.AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT, e.Message));
                    return;
                }
                _logger.WriteLine(Helper.AUTOMATA_LOAD_OK_MESSAGE);
            }

            _automataLoaded = true;

            _logger.WriteLine(Helper.AUTOMATA_LOADING_COMPLETED_MESSAGE);

        }

        private void countButton_Click(object sender, EventArgs e)
        {
            string[] patterns = new string[] { "987", "99787", "87799", "977" };
            string haystack = wordTextBox.Text;

            if (!_automataLoaded)
            {
                return;
            }

            _logger.WriteLine(string.Format(Helper.SCAN_HAYSTACK_MESSAGE_FMT, haystack));

            if (enfaRadioButton.Checked)
            {
                foreach (var pattern in patterns)
                {
                    _logger.WriteLine(string.Format(Helper.PATTERN_COUNT_MESSAGE_FMT, pattern, _nfas[pattern].CountOccurences(haystack)));
                }
            }
            else
            {
                foreach (var pattern in patterns)
                {
                    _logger.WriteLine(string.Format(Helper.PATTERN_COUNT_MESSAGE_FMT, pattern, _dfas[pattern].CountOccurences(haystack)));
                }
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            Hide();
        }
    }
}
