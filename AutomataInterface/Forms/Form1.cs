﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AutomataLibrary.Automata;
using AutomataLibrary.Builder;

namespace AutomataInterface.Forms
{
    public partial class Form1 : Form
    {
        private Logger _logger = null;
        private IFiniteAutomata _automata = null;

        private const string AUTOMATA_NOT_LOADED_ERROR_MESSAGE = "No automata is loaded. Please load automata.",
                             AUTOMATA_NOT_LOADED_HEADER = "Automata not loaded!";

        public Form1()
        {
            InitializeComponent();
            _logger = new Logger(logTextBox);
            _LoadAutomata();
        }

        private void _LoadAutomata()
        {
            string dfa_path = "Automata/part1/dfa.txt";

            _logger.Write(string.Format(Helper.AUTOMATA_LOADING_MESSAGE_FMT, dfa_path));
            try
            {
                _automata = Helper.LoadAutomata(dfa_path, new DFABuilder());
            }
            catch (Exception e)
            {
                _logger.WriteLine(string.Format(Helper.AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT, e.Message));
                return;
            }

            _logger.WriteLine(Helper.AUTOMATA_LOAD_OK_MESSAGE);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void checkButton_Click(object sender, EventArgs e)
        {
            if (_automata == null)
            {
                return;
            }

            string word = wordTextBox.Text;

            _logger.Write(string.Format(Helper.RUN_ON_WORD_MESSAGE_FMT, word));
            bool accepted = _automata.Execute(word);
            _logger.WriteLine(accepted ? Helper.WORD_ACCEPTED_MESSAGE : Helper.WORD_NOT_ACCEPTED_MESSAGE);

        }

        private void loadMenuItem_Click(object sender, EventArgs e)
        {
            AutomataBuilder builder = null;
            var mi = sender as ToolStripMenuItem;
            var typestr = new string(mi.Name.Skip("load".Length).Take(3).ToArray());
            switch (typestr)
            {
                case "Dfa":
                    builder = new DFABuilder();
                    break;
                case "Nfa":
                    builder = new NFABuilder();
                    break;
                case "Enf":
                    builder = new eNFABuilder();
                    break;
            }
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = openFileDialog.FileName;

                _logger.WriteLine(string.Format(Helper.AUTOMATA_LOADING_MESSAGE_FMT, path));
                try
                {
                    _automata = Helper.LoadAutomata(path, builder);
                }
                catch (Exception ex)
                {
                    _logger.WriteLine(string.Format(Helper.AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT, ex.Message));
                    return;
                }
                _logger.WriteLine(Helper.AUTOMATA_LOAD_OK_MESSAGE);

                openFileDialog.FileName = "";
            }
        }

        private void hiddenCheckButton_Click(object sender, EventArgs e)
        {
           if (_automata != null)
           {
               resultLabel.Text = _automata.Execute(wordTextBox.Text).ToString();
           }
           else
           {
               MessageBox.Show(AUTOMATA_NOT_LOADED_ERROR_MESSAGE, AUTOMATA_NOT_LOADED_HEADER);
           }
        }
    }
}
