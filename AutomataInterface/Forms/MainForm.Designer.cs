﻿namespace AutomataInterface.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.part1Button = new System.Windows.Forms.Button();
            this.part2button = new System.Windows.Forms.Button();
            this.part3button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // part1Button
            // 
            this.part1Button.Location = new System.Drawing.Point(12, 12);
            this.part1Button.Name = "part1Button";
            this.part1Button.Size = new System.Drawing.Size(260, 90);
            this.part1Button.TabIndex = 0;
            this.part1Button.Text = "Part I";
            this.part1Button.UseVisualStyleBackColor = true;
            this.part1Button.Click += new System.EventHandler(this.part1Button_Click);
            // 
            // part2button
            // 
            this.part2button.Location = new System.Drawing.Point(12, 108);
            this.part2button.Name = "part2button";
            this.part2button.Size = new System.Drawing.Size(260, 72);
            this.part2button.TabIndex = 1;
            this.part2button.Text = "Part II";
            this.part2button.UseVisualStyleBackColor = true;
            this.part2button.Click += new System.EventHandler(this.part1Button_Click);
            // 
            // part3button
            // 
            this.part3button.Location = new System.Drawing.Point(12, 186);
            this.part3button.Name = "part3button";
            this.part3button.Size = new System.Drawing.Size(260, 64);
            this.part3button.TabIndex = 2;
            this.part3button.Text = "Part III";
            this.part3button.UseVisualStyleBackColor = true;
            this.part3button.Click += new System.EventHandler(this.part1Button_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.part3button);
            this.Controls.Add(this.part2button);
            this.Controls.Add(this.part1Button);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button part1Button;
        private System.Windows.Forms.Button part2button;
        private System.Windows.Forms.Button part3button;
    }
}

