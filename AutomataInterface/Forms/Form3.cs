﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AutomataLibrary.Automata;
using AutomataLibrary.Automata.Decorators;
using AutomataLibrary.Builder;

namespace AutomataInterface.Forms
{
    public partial class Form3 : Form
    {
        private Logger _logger;

        private IFiniteAutomata _enfa;
        private IFiniteAutomata _dfa;

        private bool _automataLoaded = false;

        public Form3()
        {
            InitializeComponent();

            _logger = new Logger(logTextBox);

            _LoadAutomata();
        }

        private void _LoadAutomata()
        {
            string dfa_path = "Automata/part3/dfa.txt",
                   enfa_path = "Automata/part3/enfa.txt";


                _logger.Write(string.Format(Helper.AUTOMATA_LOADING_MESSAGE_FMT, dfa_path));
                try
                {
                    _dfa = Helper.LoadAutomata(dfa_path, new DFABuilder());
                }
                catch (Exception e)
                {
                    _logger.WriteLine(string.Format(Helper.AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT, e.Message));
                    return;
                }
                _logger.WriteLine(Helper.AUTOMATA_LOAD_OK_MESSAGE);

                _logger.Write(string.Format(Helper.AUTOMATA_LOADING_MESSAGE_FMT, enfa_path));
                try
                {
                    _enfa = Helper.LoadAutomata(enfa_path, new eNFABuilder());
                }
                catch (Exception e)
                {
                    _logger.WriteLine(string.Format(Helper.AUTOMATA_LOAD_FAILED_ERROR_MESSAGE_FMT, e.Message));
                    return;
                }
                _logger.WriteLine(Helper.AUTOMATA_LOAD_OK_MESSAGE);

            _automataLoaded = true;

            _logger.WriteLine(Helper.AUTOMATA_LOADING_COMPLETED_MESSAGE);

        }

        private void countButton_Click(object sender, EventArgs e)
        {
            if (!_automataLoaded)
            {
                return;
            }

            string word = wordTextBox.Text;
            _logger.Write(string.Format(Helper.RUN_ON_WORD_MESSAGE_FMT, word));
            bool accepted = enfaRadioButton.Checked ? _enfa.Execute(word) : _dfa.Execute(word);
            _logger.WriteLine(accepted ? Helper.WORD_ACCEPTED_MESSAGE : Helper.WORD_NOT_ACCEPTED_MESSAGE);
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            Hide();
        }
    }
}
