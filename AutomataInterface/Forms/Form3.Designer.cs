﻿namespace AutomataInterface.Forms
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.wordLabel = new System.Windows.Forms.Label();
            this.countButton = new System.Windows.Forms.Button();
            this.wordTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dfaRadioButton = new System.Windows.Forms.RadioButton();
            this.enfaRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(12, 151);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTextBox.Size = new System.Drawing.Size(260, 90);
            this.logTextBox.TabIndex = 9;
            // 
            // wordLabel
            // 
            this.wordLabel.Location = new System.Drawing.Point(12, 56);
            this.wordLabel.Name = "wordLabel";
            this.wordLabel.Size = new System.Drawing.Size(75, 20);
            this.wordLabel.TabIndex = 7;
            this.wordLabel.Text = "Word";
            this.wordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // countButton
            // 
            this.countButton.Location = new System.Drawing.Point(12, 82);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(260, 63);
            this.countButton.TabIndex = 6;
            this.countButton.Text = "Check";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // wordTextBox
            // 
            this.wordTextBox.Location = new System.Drawing.Point(98, 56);
            this.wordTextBox.Name = "wordTextBox";
            this.wordTextBox.Size = new System.Drawing.Size(174, 20);
            this.wordTextBox.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dfaRadioButton);
            this.groupBox1.Controls.Add(this.enfaRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 49);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // dfaRadioButton
            // 
            this.dfaRadioButton.Location = new System.Drawing.Point(133, 19);
            this.dfaRadioButton.Name = "dfaRadioButton";
            this.dfaRadioButton.Size = new System.Drawing.Size(121, 24);
            this.dfaRadioButton.TabIndex = 1;
            this.dfaRadioButton.TabStop = true;
            this.dfaRadioButton.Text = "DFA";
            this.dfaRadioButton.UseVisualStyleBackColor = true;
            // 
            // enfaRadioButton
            // 
            this.enfaRadioButton.Checked = true;
            this.enfaRadioButton.Location = new System.Drawing.Point(6, 19);
            this.enfaRadioButton.Name = "enfaRadioButton";
            this.enfaRadioButton.Size = new System.Drawing.Size(121, 24);
            this.enfaRadioButton.TabIndex = 0;
            this.enfaRadioButton.TabStop = true;
            this.enfaRadioButton.Text = "eNFA";
            this.enfaRadioButton.UseVisualStyleBackColor = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.wordLabel);
            this.Controls.Add(this.countButton);
            this.Controls.Add(this.wordTextBox);
            this.Name = "Form3";
            this.Text = "Form3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Label wordLabel;
        private System.Windows.Forms.Button countButton;
        private System.Windows.Forms.TextBox wordTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton dfaRadioButton;
        private System.Windows.Forms.RadioButton enfaRadioButton;
    }
}