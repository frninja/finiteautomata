﻿namespace AutomataInterface.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wordTextBox = new System.Windows.Forms.TextBox();
            this.checkButton = new System.Windows.Forms.Button();
            this.wordLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDfaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadNfaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEnfaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.hiddenCheckButton = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // wordTextBox
            // 
            this.wordTextBox.Location = new System.Drawing.Point(98, 30);
            this.wordTextBox.Name = "wordTextBox";
            this.wordTextBox.Size = new System.Drawing.Size(174, 20);
            this.wordTextBox.TabIndex = 0;
            // 
            // checkButton
            // 
            this.checkButton.Location = new System.Drawing.Point(12, 56);
            this.checkButton.Name = "checkButton";
            this.checkButton.Size = new System.Drawing.Size(75, 23);
            this.checkButton.TabIndex = 1;
            this.checkButton.Text = "Check";
            this.checkButton.UseVisualStyleBackColor = true;
            this.checkButton.Click += new System.EventHandler(this.checkButton_Click);
            // 
            // wordLabel
            // 
            this.wordLabel.Location = new System.Drawing.Point(12, 30);
            this.wordLabel.Name = "wordLabel";
            this.wordLabel.Size = new System.Drawing.Size(75, 20);
            this.wordLabel.TabIndex = 2;
            this.wordLabel.Text = "Word";
            this.wordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resultLabel
            // 
            this.resultLabel.Location = new System.Drawing.Point(95, 56);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(177, 23);
            this.resultLabel.TabIndex = 3;
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(12, 85);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTextBox.Size = new System.Drawing.Size(260, 165);
            this.logTextBox.TabIndex = 4;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(284, 24);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "menuStrip1";
            this.menuStrip.Visible = false;
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDfaMenuItem,
            this.loadNfaMenuItem,
            this.loadEnfaMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileMenuItem.Text = "File";
            // 
            // loadDfaMenuItem
            // 
            this.loadDfaMenuItem.Name = "loadDfaMenuItem";
            this.loadDfaMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.loadDfaMenuItem.Size = new System.Drawing.Size(172, 22);
            this.loadDfaMenuItem.Text = "Load &DFA";
            this.loadDfaMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // loadNfaMenuItem
            // 
            this.loadNfaMenuItem.Name = "loadNfaMenuItem";
            this.loadNfaMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.loadNfaMenuItem.Size = new System.Drawing.Size(172, 22);
            this.loadNfaMenuItem.Text = "Load &NFA";
            this.loadNfaMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // loadEnfaMenuItem
            // 
            this.loadEnfaMenuItem.Name = "loadEnfaMenuItem";
            this.loadEnfaMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.loadEnfaMenuItem.Size = new System.Drawing.Size(172, 22);
            this.loadEnfaMenuItem.Text = "Load &eNFA";
            this.loadEnfaMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Automata files|*.txt";
            // 
            // hiddenCheckButton
            // 
            this.hiddenCheckButton.Location = new System.Drawing.Point(98, 56);
            this.hiddenCheckButton.Name = "hiddenCheckButton";
            this.hiddenCheckButton.Size = new System.Drawing.Size(174, 23);
            this.hiddenCheckButton.TabIndex = 6;
            this.hiddenCheckButton.Text = "Check";
            this.hiddenCheckButton.UseVisualStyleBackColor = true;
            this.hiddenCheckButton.Visible = false;
            this.hiddenCheckButton.Click += new System.EventHandler(this.hiddenCheckButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.hiddenCheckButton);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.wordLabel);
            this.Controls.Add(this.checkButton);
            this.Controls.Add(this.wordTextBox);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox wordTextBox;
        private System.Windows.Forms.Button checkButton;
        private System.Windows.Forms.Label wordLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDfaMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem loadNfaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadEnfaMenuItem;
        private System.Windows.Forms.Button hiddenCheckButton;
    }
}