﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AutomataLibrary.Automata;
using AutomataLibrary.Parser;
using AutomataLibrary.Builder;

namespace AutomataInterface.Forms
{
    public partial class MainForm : Form
    {
        private Form1 _form1;
        private Form2 _form2;
        private Form3 _form3;

        public MainForm()
        {
            InitializeComponent();
        }

        private void _SwitchToForm(System.Windows.Forms.Form form)
        {
            if (form != null)
            {
                form.Show();
                form.Activate();
            }
        }

        private void part1Button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            int part;
            if (!int.TryParse(button.Name.Skip("part".Length).First().ToString(), out part))
            {
                return;
            }
            switch (part)
            {
                case 1:
                    if (_form1 != null)
                    {
                        _SwitchToForm(_form1);
                    }
                    else
                    {
                        _form1 = new Form1();
                        _form1.Show();
                    }
                    break;
                case 2:
                    if (_form2 != null)
                    {
                        _SwitchToForm(_form2);
                    }
                    else
                    {
                        _form2 = new Form2();
                        _form2.Show();
                    }
                    break;
                case 3:
                    if (_form3 != null)
                    {
                        _SwitchToForm(_form3);
                    }
                    else
                    {
                        _form3 = new Form3();
                        _form3.Show();
                    }
                    break;
            }
        }
    }
}
