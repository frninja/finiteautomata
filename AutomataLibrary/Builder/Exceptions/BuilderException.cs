﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Builder.Exceptions
{
    public class BuilderException : Exception
    {
        public BuilderException(string message) : base(message)
        {
        }
    }
}
