﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutomataLibrary.Parser.AST;
using AutomataLibrary.Automata;

using AutomataLibrary.Builder.Exceptions;

namespace AutomataLibrary.Builder
{
    public class eNFABuilder : AutomataBuilder
    {
        private Dictionary<State, List<State>> _epsilon_transition;
        private Dictionary<State, Dictionary<char, List<State>>> _transition_function;
        private State _initial_state;
        private SortedSet<int> _accepting_states;

        public eNFABuilder()
        {
            _epsilon_transition = new Dictionary<State, List<State>>();
            _transition_function = new Dictionary<State, Dictionary<char, List<State>>>();
            _initial_state = null;
            _accepting_states = new SortedSet<int>();
        }

        public override bool Build(AutomataNode automataNode)
        {
            bool foundInitial = false;

            // Fill IsAccepting state properties
            foreach (var transition in automataNode.Transitions)
            {
                if (transition.Source.IsAccepting)
                {
                    _accepting_states.Add(transition.Source.ID);
                }
            }

            foreach (var transition in automataNode.Transitions)
            {
                var sourceState = new State(transition.Source.ID, transition.Source.IsAccepting);
                var destStateID = new State(transition.Destination.ID, _accepting_states.Contains(transition.Destination.ID));

                // Process state properties 
                if (transition.Source.IsInitial)
                {
                    if (!foundInitial)
                    {
                        _initial_state = sourceState;
                        foundInitial = true;
                    }
                    else if (!_initial_state.Equals(sourceState))
                    {
                        // Automata should have only one initial state
                        throw new BuilderException(INVALID_INITIAL_STATE_COUNT_ERROR_MESSAGE);
                    }
                }

                // Process epsilon transitions
                if (transition.ContainsEpsilon)
                {
                    if (!_epsilon_transition.ContainsKey(sourceState))
                    {
                        _epsilon_transition.Add(sourceState, new List<State>());
                    }

                    _epsilon_transition[sourceState].Add(destStateID);
                }

                // Process transitions
                if (!_transition_function.ContainsKey(sourceState))
                {
                    _transition_function.Add(sourceState, new Dictionary<char, List<State>>());
                }

                foreach (var character in transition.Charset)
                {
                    if (!_transition_function[sourceState].ContainsKey(character))
                    {
                        _transition_function[sourceState].Add(character, new List<State>());
                    }

                    _transition_function[sourceState][character].Add(destStateID);
                }
            }

            // Check automata properties
            if (!foundInitial)
            {
                // Automata should have initial state
                throw new BuilderException(NO_INITIAL_STATE_ERROR_MESSAGE);
            }

            return true;
        }

        public override IFiniteAutomata GetAutomata()
        {
            return new eNFA(_epsilon_transition, _transition_function, _initial_state);
        }
    }
}
