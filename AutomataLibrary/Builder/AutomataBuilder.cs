﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutomataLibrary.Parser.AST;
using AutomataLibrary.Automata;

namespace AutomataLibrary.Builder
{
    public abstract class AutomataBuilder
    {
        protected const string INVALID_INITIAL_STATE_COUNT_ERROR_MESSAGE = "Finite automata should have only one initial state",
                               NO_INITIAL_STATE_ERROR_MESSAGE = "Finite automata should have exactly one initial state";

        public abstract bool Build(AutomataNode automataNode);
        public abstract IFiniteAutomata GetAutomata();
    }
}
