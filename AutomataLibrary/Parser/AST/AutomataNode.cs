﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Parser.AST
{
    public class AutomataNode : AbstractNode
    {
        private List<TransitionNode> _transitions;

        public IEnumerable<TransitionNode> Transitions { get { return _transitions.ToArray(); } }

        public AutomataNode()
        {
            _transitions = new List<TransitionNode>();
        }

        public void Add(TransitionNode transition)
        {
            _transitions.Add(transition);
        }
    }
}
