﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Parser.AST
{
    public class CharsetNode : AbstractNode
    {
        private readonly SortedSet<char> _charset;
        private readonly bool _containsEpsilon;
        public IEnumerable<char> Charset { get { return _charset.ToArray(); } }
        public bool ContainsEpsilon { get { return _containsEpsilon; } }

        public CharsetNode(IEnumerable<char> charset, bool containsEpsilon)
        {
            _charset = new SortedSet<char>(charset);
            _containsEpsilon = containsEpsilon;
        }
    }
}
