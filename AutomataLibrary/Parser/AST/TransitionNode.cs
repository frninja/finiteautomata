﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Parser.AST
{
    public class TransitionNode : AbstractNode
    {
        private readonly SourceStateNode _source_state;
        private readonly CharsetNode _charset;
        private readonly DestinationStateNode _dest_state;

        public SourceStateNode Source { get { return _source_state; } }
        public IEnumerable<char> Charset { get { return _charset.Charset.ToArray(); } }
        public bool ContainsEpsilon { get { return _charset.ContainsEpsilon; } }
        public DestinationStateNode Destination { get { return _dest_state; } }

        public TransitionNode(SourceStateNode source, CharsetNode charset, DestinationStateNode destination)
        {
            _source_state = source;
            _charset = charset;
            _dest_state = destination;
        }
    }
}
