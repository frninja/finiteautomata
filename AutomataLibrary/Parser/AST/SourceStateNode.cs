﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Parser.AST
{
    public class SourceStateNode : AbstractNode
    {
        private readonly bool _isInitial;
        private readonly bool _isAccepting;

        private readonly int _id;

        private readonly string _name;

        public bool IsInitial { get { return _isInitial; } }
        public bool IsAccepting { get { return _isAccepting; } }

        public int ID { get { return _id; } }

        public string Name { get { return _name; } }

        public SourceStateNode(bool isInitial, bool isAccepting, int id, string name)
        {
            _isInitial = isInitial;
            _isAccepting = isAccepting;
            _id = id;
            _name = name;
        }
    }
}
