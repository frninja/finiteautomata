﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Parser.AST
{
    public class DestinationStateNode : AbstractNode
    {
        private readonly int _id;
        public int ID { get { return _id; } }

        public DestinationStateNode(int id)
        {
            _id = id;
        }
    }
}
