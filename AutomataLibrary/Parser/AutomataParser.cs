﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutomataLibrary.Parser.AST;
using AutomataLibrary.Parser.Exceptions;

namespace AutomataLibrary.Parser
{
    public class AutomataParser
    {
        private const char PATH_DELIM = ';',
                           BLOCK_DELIM = ' ',
                           BLOCK_INTERNAL_DELIM = ',';

        private const string INITIAL_STATE_DEFINITION = "initial",
                             ACCEPTING_STATE_DEFINITION = "accepting",
                             EPSILON_CHARACTER = "epsilon";

        private const string SOURCE_STATE_PARSING_FAILED_ERROR_MESSAGE_FMT = "Parser error: could not parse source state {0}",
                             TRANSITION_PARSING_FAILED_ERROR_MESSAGE_FMT = "Parser error: could not parse transition {0} {1}",
                             SOURCE_STATE_INVALID_LENGTH_ERROR_MESSAGE_FMT = "Parser error: invalid length of source state {0}",
                             SOURCE_STATE_INVALID_PROPERTY_ERROR_MESSAGE_FMT = "Parser error: invalid property {0} in source state {1}",
                             SOURCE_STATE_ID_PARSING_FAILED_MESSAGE_FMT = "Parser error: could not parse id of source state {0}";

        private static AutomataParser _instance;

        protected AutomataParser()
        {
        }

        public static AutomataParser Instance()
        {
            if (_instance == null)
            {
                _instance = new AutomataParser();
            }
            return _instance;
        }

        public bool Parse(string text, out AutomataNode automataNode)
        {
            automataNode = new AutomataNode();

            var paths = text.Split(new char[] { PATH_DELIM }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var path in paths)
            {
                string trimmed_path = path.Trim();
                var blocks = trimmed_path.Split(new char[] { BLOCK_DELIM }, StringSplitOptions.RemoveEmptyEntries);

                SourceStateNode sourceStateNode;

                if (blocks.Length == 0)
                {
                    continue;
                }

                if (!_TryParseSourceState(blocks[0], out sourceStateNode))
                {
                    // Failed to parse source state
                    throw new ParserException(string.Format(SOURCE_STATE_PARSING_FAILED_ERROR_MESSAGE_FMT, blocks[0]));
                }

                // Skip state definition
                blocks = blocks.Skip(1).ToArray();

                // Add state to transitions if it have no transitions
                if (blocks.Length == 0)
                {
                    automataNode.Add(new TransitionNode(sourceStateNode, new CharsetNode(new SortedSet<char>(), false), new DestinationStateNode(-1)));
                }

                while (blocks.Length > 0)
                {
                    CharsetNode charsetNode = null;
                    DestinationStateNode destinationStateNode = null;

                    if (blocks.Length < 2 || !_TryParseTransitionDestinationPart(blocks[0], blocks[1], out charsetNode, out destinationStateNode))
                    {
                        // Failed to parse transition
                        throw new ParserException(string.Format(TRANSITION_PARSING_FAILED_ERROR_MESSAGE_FMT, blocks[0], blocks[1]));
                    }

                    automataNode.Add(new TransitionNode(sourceStateNode, charsetNode, destinationStateNode));

                    // Skip transitions and states 
                    blocks = blocks.Skip(2).ToArray();
                }

            }

            return true;
        }

        private bool _TryParseSourceState(string stateText, out SourceStateNode sourceStateNode) 
        {
            sourceStateNode = null;

            stateText = stateText.Trim();
            if (stateText.StartsWith("(") && stateText.EndsWith(")"))
            {
                stateText = stateText.Trim(new char[] { '(', ')' });
                var defs = stateText.Split(new char[] { BLOCK_INTERNAL_DELIM }, StringSplitOptions.RemoveEmptyEntries);

                if (defs.Length == 0 || defs.Length > 4)
                {
                    // Invalid source state block length
                    throw new ParserException(string.Format(SOURCE_STATE_INVALID_LENGTH_ERROR_MESSAGE_FMT, stateText));
                }

                bool isInitial = false, isAccepting = false;
                int stateID;
                string stateName = null;

                // Parse initial/accepting stateText options
                if (_TryParseStateProperties(defs[0], ref isInitial, ref isAccepting))
                {
                    defs = defs.Skip(1).ToArray();
                }

                if (defs.Length > 0 && _TryParseStateProperties(defs[0], ref isAccepting, ref isAccepting))
                {
                    defs = defs.Skip(1).ToArray();
                }

                // Parse stateText ID
                if (defs.Length == 0 || !_TryParseStateID(defs[0], out stateID))
                {
                    throw new ParserException(string.Format(SOURCE_STATE_ID_PARSING_FAILED_MESSAGE_FMT, stateText));
                }

                defs = defs.Skip(1).ToArray();

                // Parse stateText name
                if (defs.Length > 0 && _TryParseStateName(defs[0], ref stateName))
                {
                    // Nothing
                }

                sourceStateNode = new SourceStateNode(isInitial, isAccepting, stateID, stateName);
                return true;
            }
            
            return false;
        }

        private bool _TryParseCharset(string chars_text, out CharsetNode charsetNode)
        {
            SortedSet<char> charset = new SortedSet<char>();
            charsetNode = new CharsetNode(charset, false);

            chars_text = chars_text.Trim();
            if (chars_text.StartsWith("{") && chars_text.EndsWith("}"))
            {
                chars_text = chars_text.Trim(new char[] { '{', '}' });

                var chars = chars_text.Split(new char[] { BLOCK_INTERNAL_DELIM }, StringSplitOptions.RemoveEmptyEntries);

                if (chars.Length == 0)
                {
                    return false;
                }

                bool containsEpsilon = false;

                foreach (var s in chars)
                {
                    
                    if (s.Equals(EPSILON_CHARACTER, StringComparison.CurrentCultureIgnoreCase)) 
                    {
                        containsEpsilon = true;
                    }
                    else if (s.Length > 1)
                    {
                        return false;
                    }
                    else
                    {
                        charset.Add(s.First());
                    }
                }

                charsetNode = new CharsetNode(charset, containsEpsilon);

                return true;
            }
            return false;
        }

        private bool _TryParseDestinationState(string stateText, out DestinationStateNode destinationStateNode)
        {
            destinationStateNode = null;
            stateText = stateText.Trim();
            if (stateText.StartsWith("(") && stateText.EndsWith(")"))
            {
                stateText = stateText.Trim(new char[] { '(', ')' });
                int stateID;
                if (_TryParseStateID(stateText, out stateID))
                {
                    destinationStateNode = new DestinationStateNode(stateID);
                    return true;
                }
            }
            return false;
        }

        private bool _TryParseTransitionDestinationPart(string charsetText, string destinationStateText, out CharsetNode charsetNode, out DestinationStateNode destinationStateNode)
        {
            if (!_TryParseCharset(charsetText, out charsetNode))
            {
                destinationStateNode = null;
                return false;
            }

            if (!_TryParseDestinationState(destinationStateText, out destinationStateNode))
            {
                return false;
            }

            return true;
        }

        // Parser helpers

        private bool _TryParseStateProperties(string state_prop_text, ref bool isInitial, ref bool isAccepting)
        {
            if (state_prop_text.Equals(INITIAL_STATE_DEFINITION, StringComparison.CurrentCultureIgnoreCase))
            {
                isInitial = true;
                return true;
            }
            else if (state_prop_text.Equals(ACCEPTING_STATE_DEFINITION, StringComparison.CurrentCultureIgnoreCase))
            {
                isAccepting = true;
                return true;
            }
            return false;
        }

        private bool _TryParseStateID(string state_id_text, out int state_id)
        {
            int res;
            if (int.TryParse(state_id_text, out res) && res >= 0)
            {
                state_id = res;
                return true;
            }
            state_id = -1;
            return false;
        }

        private bool _TryParseStateName(string state_name_text, ref string state_name)
        {
            state_name = state_name_text;
            return true;
        }
    }
}
