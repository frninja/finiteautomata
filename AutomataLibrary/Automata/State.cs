﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata
{
    public class State
    {
        private int _id;
        private bool _is_accepting;

        public int ID { get { return _id; } }
        public bool IsAccepting { get { return _is_accepting; } }

        public State(int id, bool is_accepting)
        {
            _id = id;
            _is_accepting = is_accepting;
        }

        public override bool Equals(object obj)
        {
            State state = obj as State;
            if (state == null)
                return false;
            return (_id == state._id) && (_is_accepting == state._is_accepting);
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode() ^ _is_accepting.GetHashCode();
        }
    }
}
