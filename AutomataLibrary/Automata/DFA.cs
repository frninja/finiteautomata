﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata
{
    public class DFA : NFA, IFiniteAutomata
    {
        protected new Dictionary<State, Dictionary<char, State>> _transition_function;

        public DFA(Dictionary<State, Dictionary<char, State>> transition_function, State initial_state)
            : base(null, initial_state)
        {
            _transition_function = transition_function;
        }

        public DFA(DFA dfa) : base(null, dfa._initial_state)
        {
            _transition_function = dfa._transition_function;
        }

        public new bool Execute(string word)
        {
            var state = _initial_state;
            for (int offset = 0; offset < word.Length; ++offset)
            {
                if (_transition_function[state].ContainsKey(word[offset]))
                {
                    state = _transition_function[state][word[offset]];
                }
                else
                {
                    // There is no transition by this character -> word is not accepted
                    return false;
                }
            }

            if (state.IsAccepting)
            {
                // We're in accepting state (and word reading completed) -> word is accepted
                return true;
            }

            return false;
        }

    }
}
