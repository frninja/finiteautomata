﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata
{
    public class eNFA : NFA, IFiniteAutomata
    {
        protected Dictionary<State, List<State>> _epsilon_transitions;
        public eNFA(Dictionary<State, List<State>> epsilon_transitions, Dictionary<State, Dictionary<char, List<State>>> transition_function, State initial_state)
            : base(transition_function, initial_state)
        {
            _epsilon_transitions = epsilon_transitions;
        }

        public eNFA(eNFA enfa) : base(enfa._transition_function, enfa._initial_state)
        {
            _epsilon_transitions = enfa._epsilon_transitions;
        }

        public new bool Execute(string word)
        {
            Queue<Tuple<State, int>> clones = new Queue<Tuple<State, int>>(); // <state, offset>
            clones.Enqueue(new Tuple<State, int>(_initial_state, 0));

            while (clones.Count != 0)
            {
                var clone = clones.Dequeue();

                var clone_state = clone.Item1;
                var clone_offset = clone.Item2;

                if (clone_offset <= word.Length)
                {
                    // Epsilon transitions
                    if (_epsilon_transitions.ContainsKey(clone_state))
                    {
                        foreach (var state in _epsilon_transitions[clone_state])
                        {
                            clones.Enqueue(new Tuple<State, int>(state, clone_offset));
                        }
                    }
                }

                if (clone_offset < word.Length)
                {
                    var word_character = word[clone_offset];

                    if (_transition_function.ContainsKey(clone_state) && _transition_function[clone_state].ContainsKey(word_character))
                    {
                        foreach (var state in _transition_function[clone_state][word_character])
                        {
                            clones.Enqueue(new Tuple<State, int>(state, clone_offset + 1));
                        }
                    }
                }

                if (clone_state.IsAccepting && clone_offset == word.Length)
                {
                    // We're in accepting state and word reading completed -> word is accepted
                    return true;
                }
            }
            return false;
        }

    }
}
