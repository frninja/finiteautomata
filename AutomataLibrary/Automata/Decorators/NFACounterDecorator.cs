﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata.Decorators
{
    public class NFACounterDecorator : NFA, IFiniteAutomataCounterDecorator
    {

        public NFACounterDecorator(NFA nfa) : base(nfa)
        {
        }

        public int CountOccurences(string word)
        {
            int result = 0;

            Queue<Tuple<State, int>> clones = new Queue<Tuple<State, int>>(); // <state, offset>
            clones.Enqueue(new Tuple<State, int>(_initial_state, 0));

            while (clones.Count != 0)
            {
                var clone = clones.Dequeue();

                var clone_state = clone.Item1;
                var clone_offset = clone.Item2;

                if (clone_offset < word.Length)
                {
                    if (_transition_function.ContainsKey(clone_state))
                    {
                        if (_transition_function[clone_state].ContainsKey(word[clone_offset]))
                        {
                            foreach (var state in _transition_function[clone_state][word[clone_offset]])
                            {
                                clones.Enqueue(new Tuple<State, int>(state, clone_offset + 1));
                            }
                        }
                        else if (clone_state.Equals(_initial_state) || clone_state.IsAccepting)
                        {
                            clones.Enqueue(new Tuple<State, int>(clone_state, clone_offset + 1));
                        }
                    }
                }
                else if (clone_state.IsAccepting && clone_offset == word.Length)
                {
                    // We're in accepting state and word reading completed -> word is accepted
                    ++result;
                }
            }
            return result;
        }
    }
}
