﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata.Decorators
{
    public interface IFiniteAutomataCounterDecorator : IFiniteAutomata
    {
        int CountOccurences(string word);
    }
}
