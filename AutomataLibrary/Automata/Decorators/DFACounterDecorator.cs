﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata.Decorators
{
    public class DFACounterDecorator : DFA, IFiniteAutomataCounterDecorator
    {
        public DFACounterDecorator(DFA dfa) : base(dfa)
        {
        }

        public int CountOccurences(string word)
        {
            int result = 0;

            var state = _initial_state;
            for (int offset = 0; offset < word.Length; ++offset)
            {
                if (_transition_function[state].ContainsKey(word[offset]))
                {
                    state = _transition_function[state][word[offset]];
                }
                else
                {
                    state = _initial_state;
                }

                if (state.IsAccepting)
                {
                    // We're in accepting state -> word is accepted
                    ++result;
                }
            }

            return result;
        }
    }
}
