﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata.Decorators
{
    public class eNFACounterDecorator : eNFA, IFiniteAutomataCounterDecorator
    {
        public eNFACounterDecorator(eNFA enfa) : base(enfa)
        {
        }

        public int CountOccurences(string word)
        {
            int result = 0;

            Queue<Tuple<State, int>> clones = new Queue<Tuple<State, int>>(); // <state, offset>
            clones.Enqueue(new Tuple<State, int>(_initial_state, 0));

            while (clones.Count != 0)
            {
                var clone = clones.Dequeue();

                var clone_state = clone.Item1;
                var clone_offset = clone.Item2;

                if (clone_offset <= word.Length)
                {
                    // Epsilon transitions
                    if (_epsilon_transitions.ContainsKey(clone_state))
                    {
                        foreach (var state in _epsilon_transitions[clone_state])
                        {
                            clones.Enqueue(new Tuple<State, int>(state, clone_offset));
                        }
                    }
                }

                if (clone_offset < word.Length)
                {
                    var word_character = word[clone_offset];

                    if (_transition_function.ContainsKey(clone_state) && _transition_function[clone_state].ContainsKey(word_character))
                    {
                        foreach (var state in _transition_function[clone_state][word_character])
                        {
                            clones.Enqueue(new Tuple<State, int>(state, clone_offset + 1));
                        }
                    }
                    else if (clone_state.Equals(_initial_state) || clone_state.IsAccepting)
                    {
                        clones.Enqueue(new Tuple<State, int>(clone_state, clone_offset + 1));
                    }
                }

                if (clone_state.IsAccepting && clone_offset == word.Length)
                {
                    // We're in accepting state and word reading completed -> word is accepted
                    ++result;
                }
            }

            return result;
        }
    }
}
