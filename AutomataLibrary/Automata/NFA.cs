﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata
{
    public class NFA : IFiniteAutomata
    {
        protected State _initial_state;
        protected readonly Dictionary<State, Dictionary<char, List<State>>> _transition_function;

        public NFA(Dictionary<State, Dictionary<char, List<State>>> transition_function, State initial_state)
        {
            _transition_function = transition_function;
            _initial_state = initial_state;
        }

        public NFA(NFA nfa)
        {
            _transition_function = nfa._transition_function;
            _initial_state = nfa._initial_state;
        }

        public bool Execute(string word)
        {
            Queue<Tuple<State, int>> clones = new Queue<Tuple<State, int>>(); // <state, offset>
            clones.Enqueue(new Tuple<State, int>(_initial_state, 0));

            while (clones.Count != 0)
            {
                var clone = clones.Dequeue();

                var clone_state = clone.Item1;
                var clone_offset = clone.Item2;

                if (clone_offset < word.Length)
                {
                    if (_transition_function.ContainsKey(clone_state) && _transition_function[clone_state].ContainsKey(word[clone_offset]))
                    {
                        foreach (var state in _transition_function[clone_state][word[clone_offset]])
                        {
                            clones.Enqueue(new Tuple<State, int>(state, clone_offset + 1));
                        }
                    }
                }
                else if (clone_state.IsAccepting && clone_offset == word.Length)
                {
                    // We're in accepting state and word reading completed -> word is accepted
                    return true;
                }
            }
            return false;
        }

    }
}
