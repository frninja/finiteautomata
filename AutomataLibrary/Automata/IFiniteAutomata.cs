﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomataLibrary.Automata
{
    public interface IFiniteAutomata
    {
        bool Execute(string word);
    }
}
