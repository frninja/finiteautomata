﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.IO;
using AutomataLibrary.Automata;
using AutomataLibrary.Builder;
using AutomataLibrary.Parser;
using AutomataLibrary.Parser.AST;

namespace AutomataUnitTests
{
    public class UnitTestHelper
    {
        public static string BUILDER_INVALID_INITIAL_STATE_COUNT_ERROR_MESSAGE = "Finite automata should have only one initial state",
                             BUILDER_NO_INITIAL_STATE_ERROR_MESSAGE = "Finite automata should have exactly one initial state", 
                             BUILDER_DFA_EPSILON_TRANSITION_ERROR_MESSAGE = "DFA should not have epsilon transitions",
                             BUILDER_DFA_ONE_CHARACTER_MULTI_TRANSITION_ERROR_MESSAGE = "DFA should have only one transition for each character",
                             BUILDER_NFA_EPSILON_TRANSITION_ERROR_MESSAGE = "NFA should not have epsilon transitions";

        public static IFiniteAutomata LoadAutomata(string path, AutomataBuilder builder)
        {
            AutomataNode root_automata_node = null;
            using (var text_reader = new StreamReader(path))
            {
                bool parser_success = AutomataParser.Instance().Parse(text_reader.ReadToEnd(), out root_automata_node);

                if (parser_success)
                {
                    bool builder_success = builder.Build(root_automata_node);

                    if (builder_success)
                    {
                        return builder.GetAutomata();
                    }
                }
            }
            return null;
        }
        
        public static void TestBuild(string automata_path, AutomataBuilder builder, string expected_message)
        {
            AutomataNode root_automata_node = null;
            using (var text_reader = new StreamReader(automata_path))
            {
                bool parser_success = AutomataParser.Instance().Parse(text_reader.ReadToEnd(), out root_automata_node);

                if (parser_success)
                {
                    try
                    {
                        builder.Build(root_automata_node);
                    }
                    catch (Exception e)
                    {
                        StringAssert.Contains(e.Message, expected_message);
                    }
                }
            }
        }

        public static void TestBuildCorrect(string automata_path, AutomataBuilder builder)
        {
            AutomataNode root_automata_node = null;
            using (var text_reader = new StreamReader(automata_path))
            {
                bool parser_success = AutomataParser.Instance().Parse(text_reader.ReadToEnd(), out root_automata_node);

                if (parser_success)
                {
                    try
                    {
                        builder.Build(root_automata_node);
                    }
                    catch (Exception e)
                    {
                        Assert.Fail(string.Format("Automata build failed. Exception message: {0}", e.Message));
                    }
                }
            }
        }
    }
}
