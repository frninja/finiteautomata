﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutomataLibrary;
using AutomataLibrary.Automata;
using AutomataLibrary.Automata.Decorators;
using AutomataLibrary.Builder;
using AutomataLibrary.Parser;
using AutomataLibrary.Parser.AST;

namespace AutomataUnitTests
{
    [TestClass]
    public class SimpleEnfaTests
    {
        private const string ENFA_BUILD_ERROR_MESSAGE = "Test ENFA execute: build failed";

        private const string ENFA_BUILD_TEST_NO_INITIAL_STATE_FILENAME = "../../TestAutomata/ENFA/Build/test_no_initial_state.txt",
                             ENFA_BUILD_TEST_MULTI_INITIAL_STATE_FILENAME = "../../TestAutomata/ENFA/Build/test_multi_initial_state.txt",
            //ENFA_BUILD_TEST_MULTI_TRANSITION_FILENAME = "../../TestAutomata/ENFA/Build/test_multi_transition.txt",
            //ENFA_BUILD_TEST_EPSILON_TRANSITION_FILENAME = "../../TestAutomata/ENFA/Build/test_epsilon_transition.txt",
                             ENFA_BUILD_TEST_CORRECT_FILENAME = "../../TestAutomata/ENFA/Build/test_correct.txt",
                             ENFA_EXECUTE_TEST_ACCEPT_NOTHING = "../../TestAutomata/ENFA/Execute/Simple/test_accept_nothing.txt",
                             ENFA_EXECUTE_TEST_ACCEPT_ANY = "../../TestAutomata/ENFA/Execute/Simple/test_accept_any.txt",
                             ENFA_EXECUTE_TEST_ACCEPT_ONLY_EPSILON_FILENAME = "../../TestAutomata/ENFA/Execute/Simple/test_accept_only_epsilon.txt",
                             ENFA_COUNT_TEST_COUNT = "../../TestAutomata/ENFA/Counter/test_count.txt",
                             ENFA_COUNT_TEST_COUNT2 = "../../TestAutomata/ENFA/Counter/test_count2.txt",
                             ENFA_COUNT_TEST_COUNT3 = "../../TestAutomata/ENFA/Counter/test_count3.txt";

        private const string EMPTY_STRING = "";


        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Build_NoInitialState()
        {
            UnitTestHelper.TestBuild(ENFA_BUILD_TEST_NO_INITIAL_STATE_FILENAME, new eNFABuilder(), UnitTestHelper.BUILDER_NO_INITIAL_STATE_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Build_MultiInitialState()
        {
            UnitTestHelper.TestBuild(ENFA_BUILD_TEST_MULTI_INITIAL_STATE_FILENAME, new eNFABuilder(), UnitTestHelper.BUILDER_INVALID_INITIAL_STATE_COUNT_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Build_Correct()
        {
            UnitTestHelper.TestBuildCorrect(ENFA_BUILD_TEST_CORRECT_FILENAME, new eNFABuilder());
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Execute_AcceptNothing()
        {
            var enfa = UnitTestHelper.LoadAutomata(ENFA_EXECUTE_TEST_ACCEPT_NOTHING, new eNFABuilder());
            Assert.AreEqual(true, enfa != null, ENFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(false, enfa.Execute(EMPTY_STRING));
            Assert.AreEqual(false, enfa.Execute("010"));
            Assert.AreEqual(false, enfa.Execute("s"));
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Execute_AcceptAny()
        {
            var enfa = UnitTestHelper.LoadAutomata(ENFA_EXECUTE_TEST_ACCEPT_ANY, new eNFABuilder());
            Assert.AreEqual(true, enfa != null, ENFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(true, enfa.Execute(EMPTY_STRING));
            Assert.AreEqual(true, enfa.Execute("010"));
            Assert.AreEqual(true, enfa.Execute("11"));
            Assert.AreEqual(true, enfa.Execute("101"));
            Assert.AreEqual(true, enfa.Execute("0"));
            Assert.AreEqual(true, enfa.Execute("00"));
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Execute_AcceptOnlyEpsilon()
        {
            var enfa = UnitTestHelper.LoadAutomata(ENFA_EXECUTE_TEST_ACCEPT_ONLY_EPSILON_FILENAME, new eNFABuilder());
            Assert.AreEqual(true, enfa != null, ENFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(true, enfa.Execute(EMPTY_STRING));
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Count()
        {
            var enfa = new eNFACounterDecorator(UnitTestHelper.LoadAutomata(ENFA_COUNT_TEST_COUNT, new eNFABuilder()) as eNFA);
            //enfa = new eNFACounterDecorator(enfa);
            Assert.AreEqual(5, enfa.CountOccurences("000000"));
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Count2()
        {
            var enfa = new eNFACounterDecorator(UnitTestHelper.LoadAutomata(ENFA_COUNT_TEST_COUNT2, new eNFABuilder()) as eNFA);
            Assert.AreEqual(0, enfa.CountOccurences(EMPTY_STRING));
            Assert.AreEqual(1, enfa.CountOccurences("987"));
            Assert.AreEqual(2, enfa.CountOccurences("987987"));
            Assert.AreEqual(2, enfa.CountOccurences("9878987"));
            Assert.AreEqual(2, enfa.CountOccurences("2987987"));
            Assert.AreEqual(2, enfa.CountOccurences("2982987987"));
            Assert.AreEqual(3, enfa.CountOccurences("987987987"));
            Assert.AreEqual(3, enfa.CountOccurences("987998899879987"));
        }

        [TestMethod]
        [TestCategory("eNFA tests")]
        public void Test_eNfa_Count3()
        {
            var enfa = new eNFACounterDecorator(UnitTestHelper.LoadAutomata(ENFA_COUNT_TEST_COUNT3, new eNFABuilder()) as eNFA);
            Assert.AreEqual(0, enfa.CountOccurences(EMPTY_STRING));
            Assert.AreEqual(0, enfa.CountOccurences("0"));
            Assert.AreEqual(0, enfa.CountOccurences("1"));

            Assert.AreEqual(0, enfa.CountOccurences("01"));
            Assert.AreEqual(0, enfa.CountOccurences("10"));
            Assert.AreEqual(0, enfa.CountOccurences("010"));
            Assert.AreEqual(0, enfa.CountOccurences("101"));

            Assert.AreEqual(1, enfa.CountOccurences("00"));
            Assert.AreEqual(1, enfa.CountOccurences("11"));
            Assert.AreEqual(1, enfa.CountOccurences("0100"));
            Assert.AreEqual(1, enfa.CountOccurences("1011"));


            Assert.AreEqual(2, enfa.CountOccurences("0011"));
            Assert.AreEqual(2, enfa.CountOccurences("1100"));
            Assert.AreEqual(2, enfa.CountOccurences("00100"));
            Assert.AreEqual(2, enfa.CountOccurences("11011"));

            Assert.AreEqual(3, enfa.CountOccurences("0000"));
            Assert.AreEqual(3, enfa.CountOccurences("1111"));
            Assert.AreEqual(3, enfa.CountOccurences("000100"));
            Assert.AreEqual(3, enfa.CountOccurences("11100"));
            Assert.AreEqual(3, enfa.CountOccurences("00101100"));
        }
    }
}
