﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutomataLibrary;
using AutomataLibrary.Automata;
using AutomataLibrary.Automata.Decorators;
using AutomataLibrary.Builder;
using AutomataLibrary.Parser;
using AutomataLibrary.Parser.AST;

namespace AutomataUnitTests
{
    [TestClass]
    public class SimpleNfaTests
    {
        private const string NFA_BUILD_ERROR_MESSAGE = "Test NFA execute: build failed";

        private const string NFA_BUILD_TEST_NO_INITIAL_STATE_FILENAME = "../../TestAutomata/NFA/Build/test_no_initial_state.txt",
                             NFA_BUILD_TEST_MULTI_INITIAL_STATE_FILENAME = "../../TestAutomata/NFA/Build/test_multi_initial_state.txt",
                             //NFA_BUILD_TEST_MULTI_TRANSITION_FILENAME = "../../TestAutomata/NFA/Build/test_multi_transition.txt",
                             NFA_BUILD_TEST_EPSILON_TRANSITION_FILENAME = "../../TestAutomata/NFA/Build/test_epsilon_transition.txt",
                             NFA_BUILD_TEST_CORRECT_FILENAME = "../../TestAutomata/NFA/Build/test_correct.txt",
                             NFA_EXECUTE_TEST_ACCEPT_NOTHING = "../../TestAutomata/NFA/Execute/Simple/test_accept_nothing.txt",
                             NFA_EXECUTE_TEST_ACCEPT_ANY = "../../TestAutomata/NFA/Execute/Simple/test_accept_any.txt",
                             NFA_EXECUTE_TEST_ACCEPT_ONLY_EPSILON_FILENAME = "../../TestAutomata/NFA/Execute/Simple/test_accept_only_epsilon.txt",
                             NFA_COUNT_TEST_COUNT = "../../TestAutomata/NFA/Counter/test_count.txt",
                             NFA_COUNT_TEST_COUNT2 = "../../TestAutomata/NFA/Counter/test_count2.txt";

        private const string EMPTY_STRING = "";


        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Build_NoInitialState()
        {
            UnitTestHelper.TestBuild(NFA_BUILD_TEST_NO_INITIAL_STATE_FILENAME, new NFABuilder(), UnitTestHelper.BUILDER_NO_INITIAL_STATE_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Build_MultiInitialState()
        {
            UnitTestHelper.TestBuild(NFA_BUILD_TEST_MULTI_INITIAL_STATE_FILENAME, new NFABuilder(), UnitTestHelper.BUILDER_INVALID_INITIAL_STATE_COUNT_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Build_EpsilonTransition()
        {
            UnitTestHelper.TestBuild(NFA_BUILD_TEST_EPSILON_TRANSITION_FILENAME, new NFABuilder(), UnitTestHelper.BUILDER_NFA_EPSILON_TRANSITION_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Build_Correct()
        {
            UnitTestHelper.TestBuildCorrect(NFA_BUILD_TEST_CORRECT_FILENAME, new NFABuilder());
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Execute_AcceptNothing()
        {
            var nfa = UnitTestHelper.LoadAutomata(NFA_EXECUTE_TEST_ACCEPT_NOTHING, new NFABuilder());
            Assert.AreEqual(true, nfa != null, NFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(false, nfa.Execute(EMPTY_STRING));
            Assert.AreEqual(false, nfa.Execute("010"));
            Assert.AreEqual(false, nfa.Execute("s"));
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Execute_AcceptAny()
        {
            var nfa = UnitTestHelper.LoadAutomata(NFA_EXECUTE_TEST_ACCEPT_ANY, new NFABuilder());
            Assert.AreEqual(true, nfa != null, NFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(true, nfa.Execute(EMPTY_STRING));
            Assert.AreEqual(true, nfa.Execute("010"));
            Assert.AreEqual(true, nfa.Execute("11"));
            Assert.AreEqual(true, nfa.Execute("101"));
            Assert.AreEqual(true, nfa.Execute("0"));
            Assert.AreEqual(true, nfa.Execute("00"));
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Execute_AcceptOnlyEpsilon()
        {
            var nfa = UnitTestHelper.LoadAutomata(NFA_EXECUTE_TEST_ACCEPT_ONLY_EPSILON_FILENAME, new NFABuilder());
            Assert.AreEqual(true, nfa != null, NFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(true, nfa.Execute(EMPTY_STRING));
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Count()
        {
            var nfa = new NFACounterDecorator(UnitTestHelper.LoadAutomata(NFA_COUNT_TEST_COUNT, new NFABuilder()) as NFA);
            Assert.AreEqual(5, nfa.CountOccurences("000000"));
        }

        [TestMethod]
        [TestCategory("NFA tests")]
        public void Test_Nfa_Count2()
        {
            var nfa = new NFACounterDecorator(UnitTestHelper.LoadAutomata(NFA_COUNT_TEST_COUNT2, new NFABuilder()) as NFA);
            Assert.AreEqual(0, nfa.CountOccurences(EMPTY_STRING));
            Assert.AreEqual(1, nfa.CountOccurences("987"));
            Assert.AreEqual(2, nfa.CountOccurences("987987"));
            Assert.AreEqual(2, nfa.CountOccurences("9878987"));
            Assert.AreEqual(2, nfa.CountOccurences("2987987"));
            Assert.AreEqual(2, nfa.CountOccurences("2982987987"));
            Assert.AreEqual(3, nfa.CountOccurences("987987987"));
            Assert.AreEqual(3, nfa.CountOccurences("987998899879987"));
            Assert.AreEqual(3, nfa.CountOccurences("987jsgxjkgs987sxhkjs987sxs"));
        }
    }
}
