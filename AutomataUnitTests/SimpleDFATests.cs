﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutomataLibrary;
using AutomataLibrary.Automata;
using AutomataLibrary.Automata.Decorators;
using AutomataLibrary.Builder;
using AutomataLibrary.Parser;
using AutomataLibrary.Parser.AST;

namespace AutomataUnitTests
{
    [TestClass]
    public class SimpleDfaTests
    {
        private const string DFA_BUILD_ERROR_MESSAGE = "Test DFA execute: build failed";

        private const string DFA_BUILD_TEST_NO_INITIAL_STATE_FILENAME = "../../TestAutomata/DFA/Build/test_no_initial_state.txt",
                             DFA_BUILD_TEST_MULTI_INITIAL_STATE_FILENAME = "../../TestAutomata/DFA/Build/test_multi_initial_state.txt",
                             DFA_BUILD_TEST_MULTI_TRANSITION_FILENAME = "../../TestAutomata/DFA/Build/test_multi_transition.txt",
                             DFA_BUILD_TEST_EPSILON_TRANSITION_FILENAME = "../../TestAutomata/DFA/Build/test_epsilon_transition.txt",
                             DFA_BUILD_TEST_CORRECT_FILENAME = "../../TestAutomata/DFA/Build/test_correct.txt",
                             DFA_EXECUTE_TEST_ACCEPT_NOTHING = "../../TestAutomata/DFA/Execute/Simple/test_accept_nothing.txt",
                             DFA_EXECUTE_TEST_ACCEPT_ANY = "../../TestAutomata/DFA/Execute/Simple/test_accept_any.txt",
                             DFA_EXECUTE_TEST_ACCEPT_ONLY_EPSILON_FILENAME = "../../TestAutomata/DFA/Execute/Simple/test_accept_only_epsilon.txt",
                             DFA_COUNT_TEST_COUNT = "../../TestAutomata/DFA/Counter/test_count.txt",
                             DFA_COUNT_TEST_COUNT2 = "../../TestAutomata/DFA/Counter/test_count2.txt";

        private const string EMPTY_STRING = "";

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Build_NoInitialState()
        {
            UnitTestHelper.TestBuild(DFA_BUILD_TEST_NO_INITIAL_STATE_FILENAME, new DFABuilder(), UnitTestHelper.BUILDER_NO_INITIAL_STATE_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Build_MultiInitialState()
        {
            UnitTestHelper.TestBuild(DFA_BUILD_TEST_MULTI_INITIAL_STATE_FILENAME, new DFABuilder(), UnitTestHelper.BUILDER_INVALID_INITIAL_STATE_COUNT_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Build_MultiTransition()
        {
            UnitTestHelper.TestBuild(DFA_BUILD_TEST_MULTI_TRANSITION_FILENAME, new DFABuilder(), UnitTestHelper.BUILDER_DFA_ONE_CHARACTER_MULTI_TRANSITION_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Build_EpsilonTransition()
        {
            UnitTestHelper.TestBuild(DFA_BUILD_TEST_EPSILON_TRANSITION_FILENAME, new DFABuilder(), UnitTestHelper.BUILDER_DFA_EPSILON_TRANSITION_ERROR_MESSAGE);
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Build_Correct()
        {
            UnitTestHelper.TestBuildCorrect(DFA_BUILD_TEST_CORRECT_FILENAME, new DFABuilder());
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Execute_AcceptNothing()
        {
            var dfa = UnitTestHelper.LoadAutomata(DFA_EXECUTE_TEST_ACCEPT_NOTHING, new DFABuilder());
            Assert.AreEqual(true, dfa != null, DFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(false, dfa.Execute(EMPTY_STRING));
            Assert.AreEqual(false, dfa.Execute("010"));
            Assert.AreEqual(false, dfa.Execute("s"));
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Execute_AcceptAny()
        {
            var dfa = UnitTestHelper.LoadAutomata(DFA_EXECUTE_TEST_ACCEPT_ANY, new DFABuilder());
            Assert.AreEqual(true, dfa != null, DFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(true, dfa.Execute(EMPTY_STRING));
            Assert.AreEqual(true, dfa.Execute("010"));
            Assert.AreEqual(true, dfa.Execute("11"));
            Assert.AreEqual(true, dfa.Execute("101"));
            Assert.AreEqual(true, dfa.Execute("0"));
            Assert.AreEqual(true, dfa.Execute("00"));
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Execute_AcceptOnlyEpsilon()
        {
            var dfa = UnitTestHelper.LoadAutomata(DFA_EXECUTE_TEST_ACCEPT_ONLY_EPSILON_FILENAME, new DFABuilder());
            Assert.AreEqual(true, dfa != null, DFA_BUILD_ERROR_MESSAGE);
            Assert.AreEqual(true, dfa.Execute(EMPTY_STRING));
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Count()
        {
            var dfa = new DFACounterDecorator(UnitTestHelper.LoadAutomata(DFA_COUNT_TEST_COUNT, new DFABuilder()) as DFA);
            Assert.AreEqual(5, dfa.CountOccurences("000000"));
        }

        [TestMethod]
        [TestCategory("DFA tests")]
        public void Test_Dfa_Count2()
        {
            var dfa = new DFACounterDecorator(UnitTestHelper.LoadAutomata(DFA_COUNT_TEST_COUNT2, new DFABuilder()) as DFA);
            Assert.AreEqual(0, dfa.CountOccurences(EMPTY_STRING));
            Assert.AreEqual(1, dfa.CountOccurences("987"));
            Assert.AreEqual(2, dfa.CountOccurences("987987"));
            Assert.AreEqual(2, dfa.CountOccurences("9878987"));
            Assert.AreEqual(2, dfa.CountOccurences("2987987"));
            Assert.AreEqual(2, dfa.CountOccurences("2982987987"));
            Assert.AreEqual(3, dfa.CountOccurences("987987987"));
            Assert.AreEqual(3, dfa.CountOccurences("987998899879987"));
        }
    }
}
